import React from "react";
import { GameEmpatContainer } from "./style";

class GameEmpat extends React.Component {
  render() {
    const { wrongAnswer, correctAnswer } = this.props;
    const wrongAnswerFunc = e => {
      const outsideAnswerArea = !document
        .getElementById("angka")
        .contains(e.target);
      if (outsideAnswerArea) {
        wrongAnswer();
      }
    };

    return (
      <GameEmpatContainer>
        <div id="main-container" onClick={wrongAnswerFunc}>
          <div className="txt-box">
            <span className="tulisan" id="desktop">
              Go to
            </span>
            <span className="tulisan"> level</span>
            <span className="tulisan" id="angka" onClick={correctAnswer}>
              {" "}
              4
            </span>
            <span className="tulisan">!</span>
          </div>
        </div>
      </GameEmpatContainer>
    );
  }
}

GameEmpat.propTypes = {};

export default GameEmpat;
