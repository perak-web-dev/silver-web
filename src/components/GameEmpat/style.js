import styled from "styled-components";
import gameempat from "../../asset/gameempatdesktop.svg";
import gameempatmobile from "../../asset/gameempatmobile.svg";

export const GameEmpatContainer = styled.div`
  #main-container {
    background-image: url(${gameempat});
    background-size: contain;
    background-repeat: no-repeat;
    background-color: #0d2040;
    background-position: center;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .txt-box {
    transform: rotate(-1deg);
  }
  .tulisan {
    font-family: "Montserrat", sans-serif;
    color: #f2cf35;
    font-size: 4vw;
    font-weight: 800;
    text-align: center;
  }

  @media screen and (max-width: 768px) {
    #main-container {
      background-image: url(${gameempatmobile});
    }

    #desktop {
      display: none;
    }

    .tulisan {
      font-size: 11vw;
    }

    .txt-box {
      transform: rotate(3deg);
    }
  }
`;
