import styled from "styled-components";
import perakLogo from "../../asset/GameKefas/perak-logo.svg";
import perakLogoMobile from "../../asset/GameKefas/perak-logo-2.svg";

export const GameNo5Container = styled.div`
  background-image: url(${perakLogo});
  background-repeat: no-repeat;
  background-position: center;
  background-color: #e5e6de;

  .mobile-container {
    display: none;
  }

  .game2-container {
    min-height: 100vh;
    width: 100vw;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .wrong-answer {
    height: 100vh;
    width: 100vw;
    position: absolute;
    left: 0;
    outline: none;
    border: none;
    background: none;
  }
  .answer {
    position: relative;
    border: none;
    background: none;
    color: #333333;
    font-weight: bold;
    outline: none;
    display: flex;
    justify-content: center;
  }
  .top-container {
    margin-top: 50px;
  }
  .vertical-container {
    width: 100%;
  }
  .horizontal-container {
    width: 100%;
    display: flex;
    justify-content: space-between;
    padding: 0px 10%;
  }

  @media only screen and (max-width: 1000px) {
    .img-answer {
      transform: scale(0.7, 0.7);
    }
    .horizontal-container {
      width: 100%;
      display: flex;
      justify-content: space-between;
      padding: 0px 0px;
    }
  }
  @media only screen and (max-width: 700px) {
    background-position-y: 230px;
    background-image: url(${perakLogoMobile});
    .answer h1 {
      width: 80%;
    }
    .pc-container {
      display: none;
    }
    .mobile-container {
      display: block;
      width: 100%;
    }
    .img-answers-mobile {
      margin-top: 170px;
      width: 100%;
    }
  }
`;
