import React from "react";
import hijau from "../../asset/GameKefas/hijau.svg";
import kuning from "../../asset/GameKefas/kuning.svg";
import orange from "../../asset/GameKefas/orange.svg";
import biru from "../../asset/GameKefas/biru.svg";
import answersMobile from "../../asset/GameKefas/answersMobile.svg";

import { GameNo5Container } from "./style";

class GameNo5 extends React.Component {
  render() {
    const { wrongAnswer, correctAnswer } = this.props;

    return (
      <GameNo5Container>
          <button
            onClick={wrongAnswer}
            type="button"
            className="wrong-answer"
            id="wrong-answer"
          ></button>
          <div className="game2-container">
            <div className="top-container">
              <button type="button" onClick={correctAnswer} className="answer">
                <h1>Manakah yang bukan warna dari PERAK?</h1>
              </button>
            </div>
            <div className="vertical-container pc-container">
              <div className="horizontal-container">
                <img className="img-answer" src={hijau} alt="hijau"></img>
                <img className="img-answer" src={kuning} alt="kuning"></img>
              </div>
              <div className="horizontal-container">
                <img className="img-answer" src={orange} alt="orange"></img>
                <img className="img-answer" src={biru} alt="biru"></img>
              </div>
            </div>
            <div className="vertical-container mobile-container">
              <img
                className="img-answers-mobile"
                src={answersMobile}
                alt="answer-mobile"
              ></img>
            </div>
          </div>
      </GameNo5Container>
    );
  }
}

export default GameNo5;
