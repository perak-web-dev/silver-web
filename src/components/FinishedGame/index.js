import React from "react";

import { Link } from "react-router-dom";
import {
  Congrats,
  FinishedGameContainer,
  FinishedInnerContainer,
  BackButton,
  BackButtonWrapper
} from "./style";
import congrats from "../../asset/congratsDone.svg";
import HeaderFooter from "../HeaderFooter";

class FinishedGame extends React.PureComponent {
  render() {
    const { name, loading } = this.props;
    return (
      <HeaderFooter color="dark" noQuiz>
        <FinishedGameContainer>
          <FinishedInnerContainer>
            <div
              style={{
                alignSelf: "center",
                width: "100%",
                maxWidth: "inherit"
              }}
            >
              {loading && <Congrats>Mengirim data permainan....</Congrats>}
              {!loading && (
                <Congrats>Terima kasih, {name} telah bermain</Congrats>
              )}
            </div>
            <img
              src={congrats}
              alt="congrats"
              style={{
                maxWidth: "inherit",
                width: "100%",
                height: "auto",
                padding: "0 10%"
              }}
            />
            <BackButtonWrapper>
              <Link to="/">
                <BackButton>Kembali ke Homepage</BackButton>
              </Link>
            </BackButtonWrapper>
          </FinishedInnerContainer>
        </FinishedGameContainer>
      </HeaderFooter>
    );
  }
}

export default FinishedGame;
