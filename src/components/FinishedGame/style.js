import styled from "styled-components";

export const FinishedGameContainer = styled.div`
  background: #f2cf35;
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  font-family: Montserrat, serif;
`;

export const FinishedInnerContainer = styled.div`
  align-self: center;
  display: flex;
  flex-direction: column;
  max-width: 1000px;
  width: 100%;
`;

export const BackButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  a {
    text-decoration: none;
  }
`;

export const BackButton = styled.div`
  margin-top: 20px;
  color: #e5e6de;
  background-color: #0d2040;
  padding: 10px 20px;
  border-radius: 8px;
  font-family: Montserrat;
  font-weight: bold;
  font-size: 1em;
`;

export const Congrats = styled.h2`
  font-size: 2rem;
  text-align: center;
  font-weight: bold;
  color: #e9622a;
  margin-bottom: 1rem;

  @media screen and (max-width: 500px) {
    font-size: 1.5rem;
  }
`;
