import React from "react";
import { GameEnamStyled } from "./style";
import gameenamicon from "../../asset/gameenamicon.svg";

const GameEnam = ({ correctAnswer, wrongAnswer }) => {
  const wrongAnswerFunc = e => {
    const outsideAnswerArea = !document
      .getElementById("span")
      .contains(e.target);
    if (outsideAnswerArea) {
      wrongAnswer();
    }
  };

  return (
    <GameEnamStyled>
      <div id="game-6" onClick={wrongAnswerFunc}>
        <div className="title">
          <img src={gameenamicon} alt="sirine" />
          <h3>No mer enam</h3>
        </div>
        <p className="text">
          Di suatu pagi yang cerah, ijug bangun dari tidur nyenyaknya. Pagi ini
          ia memutuskan untuk mencari sarapan nasi uduk. Tetapi, karena ijug
          adalah seorang mahasiswa, ia tidak mau menghabiskan seluruh uang
          jajannya untuk sekali makan saja.
        </p>
        <p className="text">
          Sesampainya di warteg, ijug langsung memesan nasi uduk untuk
          dibungkus. Ia menyebutkan lauknya, dan tidak lupa membungkus satu lagi
          untuk ibunya di rumah.
        </p>
        <p className="text left-align">Yuk, bantu Ijug! Carilah a =</p>
        <div className="option-row">
          <p className="text option">
            <span id="span" onClick={correctAnswer}>
              a
            </span>
            . 56
          </p>
          <p className="text option">b. 57</p>
        </div>
        <div className="option-row">
          <p className="text option">c. 58</p>
          <p className="text option">d. 59</p>
        </div>
      </div>
    </GameEnamStyled>
  );
};

export default GameEnam;
