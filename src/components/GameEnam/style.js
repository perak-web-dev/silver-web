import styled from "styled-components";
import gameenam from "../../asset/gameenam.png";
import gameenammobile from "../../asset/gameenammobile.png";

export const GameEnamStyled = styled.div`
  @import url("https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&family=Press+Start+2P&display=swap");

  #game-6 {
    background-image: url(${gameenam});
    background-size: cover;
    background-position: center;
    height: 100vh;
    padding: 40px 150px;
  }

  .title {
    width: fit-content;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 50px;
  }

  .title > h3 {
    font-family: "Press Start 2P", cursive;
    font-size: 32px;
    line-height: 32px;
    text-align: center;
    color: #323137;
  }
  .text {
    font-family: "Montserrat", sans-serif;
    color: #000;
    font-size: 20px;
    line-height: 24px;
    text-align: justify;
    margin-bottom: 15px;
  }
  .option {
    margin-bottom: 10px;
  }

  /* Ipad */
  @media only screen and (max-width: 780px) {
    #game-6 {
      background-image: url(${gameenammobile}) !important;
      background-position: bottom;
    }
  }

  /* Mobile */
  @media only screen and (max-width: 430px) {
    #game-6 {
      padding: 50px 40px;
      display: flex;
      flex-direction: column;
      align-items: center;
    }
    .title {
      margin-bottom: 20px;
    }
    .title > h3 {
      font-size: 22px;
    }
    .text {
      font-size: 16px;
      line-height: 20px;
    }
    .left-align {
      align-self: flex-start;
      margin-bottom: 20px;
    }
    .option {
      font-size: 22px;
      font-weight: 700;
    }
    .option-row {
      display: flex;
      justify-content: space-around;
      width: 100%;
      margin: 10px 0;
    }
  }

  /* XS Mobile */
  @media only screen and (max-width: 430px) and (max-height: 700px) {
    #game-6 {
      height: fit-content;
      padding-bottom: 120px;
    }
  }
`;
