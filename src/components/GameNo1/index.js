import React from "react";

import { GameNo1Container } from "./style";
import egg from "../../asset/GameKefas/egg.svg";
import eggsMobile from "../../asset/GameKefas/eggs-mobile.svg";

class GameNo1 extends React.Component {
  render() {
    function sleep(time) {
      return new Promise(resolve => setTimeout(resolve, time));
    }

    const benarFunction = () => {
      const { correctAnswer } = this.props;
      const telurBenar = document.getElementById("telur-benar");
      telurBenar.style.transition = "0.3s ease-in-out";
      telurBenar.style.transform = "scale(2, 2)";
      sleep(300).then(() => {
        telurBenar.style.transition = "0s";
        telurBenar.style.opacity = 0;
        correctAnswer();
      });
    };

    const salahFunction = () => {
      const { wrongAnswer } = this.props;
      wrongAnswer();
    };

    return (
      <GameNo1Container>
        <button
          onClick={salahFunction}
          type="button"
          className="wrong-answer"
          id="wrong-answer"
        ></button>
        <div className="game1-container">
          <h1 className="text-no1">
            Mari coba keberuntunganmu! Pecahkanlah{" "}
            <button
              onClick={benarFunction}
              type="button"
              className="telur-benar"
              id="telur-benar"
            >
              <span id="text-telur">telur</span>{" "}
              <span id="text-yang-benar">yang benar</span>
            </button>{" "}
            untuk menuju level selanjutnya!
          </h1>
          <div className="egg-container">
            <img src={egg} className="egg" alt="egg"></img>
            <img src={egg} className="egg" alt="egg"></img>
            <img src={egg} className="egg" alt="egg"></img>
            <img src={egg} className="egg" alt="egg"></img>
          </div>
          <img src={eggsMobile} className="eggs-mobile" alt="eggs"></img>
        </div>
      </GameNo1Container>
    );
  }
}

GameNo1.propTypes = {};

export default GameNo1;
