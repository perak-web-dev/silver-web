import styled from "styled-components";
import bg1 from "../../asset/GameKefas/bg-1.svg";
import bg1Mobile from "../../asset/GameKefas/bg-1-mobile.svg";

export const GameNo1Container = styled.div`
  .wrong-answer {
    height: 100vh;
    width: 100vw;
    position: absolute;
    left: 0;
    outline: none;
    border: none;
    background: none;
  }
  .game1-container {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100vw;
    min-height: 100vh;
    background-color: #cafafa;
    background-image: url(${bg1});
    background-repeat: no-repeat;
    background-position: bottom;
    background-size: 100%;
  }
  .eggs-mobile {
    display: none;
  }
  .text-no1 {
    margin-top: 116px;
    color: #e9622a;
    font-size: 40px;
    text-align: center;
    max-width: 786px;
  }
  .egg-container {
    margin-top: 207px;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  }
  .egg {
    margin: 0px 10px;
  }
  .telur-benar {
    position: relative;
    border: none;
    background: none;
    color: #e9622a;
    font-weight: bold;
    outline: none;
  }
  @media only screen and (max-width: 900px) {
    .text-no1 {
      margin-top: 60px;
    }
    .game1-container {
      padding: 0px 30px;
    }
    .egg {
      transform: scale(0.9, 0.9);
    }
    .egg-container {
      margin-top: 70px;
    }
  }

  @media only screen and (max-width: 600px) {
    .text-no1 {
      margin-top: 60px;
      font-size: 22px;
    }
    .game1-container {
      padding: 0px 30px;
      background-image: url(${bg1Mobile});
    }
    .egg {
      transform: scale(0.7, 0.7);
      margin: 0px 0px;
    }
    .egg-container {
      margin-top: 30px;
    }
  }
  @media only screen and (max-width: 460px) {
    .egg-container {
      display: none;
    }
    .eggs-mobile {
      margin-top: 38px;
      display: block;
      width: 100%;
    }
  }
`;
