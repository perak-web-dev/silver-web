import styled from "styled-components";
import no10 from "./no10.svg";
import a from "./a.svg";
import b from "./b.svg";
import c from "./c.svg";
import d from "./d.svg";
import e from "./e.svg";
import f from "./f.svg";

const courts = {
  a,
  b,
  c,
  d,
  e,
  f
};

export const CourtContainer = styled.div`
  background: url(${props => courts[props.type]});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  display: flex;
  justify-content: center;
  position: relative;
  h1 {
    position: absolute;
    margin-top: 23vh;
    margin-left: -6.5vw;
    @media (max-width: 1200px) {
      margin-top: 15vh;
      margin-left: -10vw;
    }
    @media (max-width: 800px) {
      right: 70%;
      margin-top: 15vh;
      margin-left: -27.5vw;
    }
  }
  img {
    display: inline-block;
    cursor: pointer;
    margin: auto;
  }
`;

const color = {
  yellow: "#F2CF35",
  purple: "#444fcb",
  orange: "#E9622A",
  green: "#33B3A6"
};

export const Container = styled.div`
background: ${color.purple}
color: ${color.yellow}
padding: 10px;
font-weight: bold;
font-size: calc(1vw + 1rem);
position: absolute;
border-radius: 2px;
left: 50vw;
transform: translate(-50%);
z-index: 10;
@media (min-width: 1200px) {
  top: 53vh;
}
`;

export const Parampa10Container = styled.div`
  // background: url(${no10});
  // background-position: center;
  height: 100vh;
  display: grid;
  @media (max-width: 1200px) {
    grid-template-columns: auto auto;
  }
  grid-template-columns: auto auto auto;
`;

export const ModalContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  background: url(${no10});
  background-position: center;
`;
