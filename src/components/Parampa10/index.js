/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from "react";
import bola from "./bola.svg";
import bolb from "./bolb.svg";
import bolc from "./bolc.svg";
import bold from "./bold.svg";
import bole from "./bole.svg";
import bolf from "./bolf.svg";
import {
  Parampa10Container,
  CourtContainer,
  Container
  // ModalContainer
} from "./style";
// import { Modal } from "../Parampa8/style";

const balls = {
  a: bola,
  b: bolb,
  c: bolc,
  d: bold,
  e: bole,
  f: bolf
};

const Court = ({ ball, correctAnswer }) => {
  const handleAnswer = e => {
    e.stopPropagation();
    correctAnswer();
  };
  return (
    <CourtContainer type={ball}>
      <h1 onClick={ball === "e" ? handleAnswer : null} >{ball.toUpperCase()}.</h1>
      {ball === "e" ? (
        <img src={balls[ball]} alt={ball} onClick={handleAnswer} />
      ) : (
        <img src={balls[ball]} alt={ball} />
      )}
    </CourtContainer>
  );
};

const Parampa10 = ({ correctAnswer, wrongAnswer }) => {
  // const [modal, setModal] = useState(false);
  // function timeout(delay) {
  //   return new Promise(res => setTimeout(res, delay));
  // }
  // const handleWrong = async () => {
  //   setModal(true);
  //   await timeout(2500);
  //   setModal(false);
  // };
  return (
    <Parampa10Container onClick={wrongAnswer}>
      {/* {modal ? (
        <ModalContainer>
          <Modal open={modal}>
            <img
              src="https://i.pinimg.com/originals/50/8f/e7/508fe7a0908b87ea1de36391094a1049.gif"
              alt="youshallnotpass"
            />
          </Modal>
        </ModalContainer>
      ) : ( */}
      <>
        <Container>Pilihlah bola yang paling berbahaya!</Container>
        <Court ball="a" correctAnswer={correctAnswer} />
        <Court ball="b" correctAnswer={correctAnswer} />
        <Court ball="c" correctAnswer={correctAnswer} />
        <Court ball="d" correctAnswer={correctAnswer} />
        <Court ball="e" correctAnswer={correctAnswer} />
        <Court ball="f" correctAnswer={correctAnswer} />
      </>
      {/* )} */}
    </Parampa10Container>
  );
};

Parampa10.propTypes = {};

export default Parampa10;
