import styled from "styled-components";
import background from "../../asset/gametujuhBG.svg";

export const GameTujuhContainer = styled.div`
  #main-container {
    background-image: url(${background});
    background-size: cover;
    background-position: center;
    height: 100vh;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .tulisan {
    font-family: "Montserrat", sans-serif;
    color: #f2cf35;
    font-size: 40px;
    font-weight: 700;
    text-align: center;
  }

  #box-inside {
    width: 90%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  #box-choice {
    width: 100%;
    padding: 2vh 0;
  }

  .row {
    display: flex;
    flex-direction: row;
    width: 100%;
    justify-content: center;
  }

  #atas {
    margin: 5vh 0;
  }

  #bawah {
    margin: 5vh 0;
  }

  .image {
    width: 12vw;
    height: auto;
    margin: 0 4vw;
    border-radius: 50%;
  }

  @media screen and (max-width: 768px) {
    .tulisan {
      font-size: 32px;
    }

    #box-choice {
      margin: 2vh 0;
    }

    #atas {
      flex-direction: column;
      align-items: center;
      margin: 0;
    }

    #bawah {
      flex-direction: column;
      align-items: center;
      margin: 0;
    }

    .image {
      width: 23vw;
      height: auto;
      margin: 2vh 0;
    }
  }
`;
