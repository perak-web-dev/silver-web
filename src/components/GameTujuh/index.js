import React from "react";
import choiceB from "../../asset/gametujuhB.svg";
import choiceC from "../../asset/gametujuhC.svg";
import choiceD from "../../asset/gametujuhD.svg";
import choiceE from "../../asset/gametujuhE.svg";

import { GameTujuhContainer } from "./style";

class GameTujuh extends React.Component {
  render() {
    const { correctAnswer, wrongAnswer } = this.props;
    const wrongAnswerFunc = e => {
      const outsideAnswerArea = !document
        .getElementById("choice-b")
        .contains(e.target);
      if (outsideAnswerArea) {
        wrongAnswer();
      }
    };

    return (
      <GameTujuhContainer>
        <div id="main-container" onClick={wrongAnswerFunc}>
          <div id="box-inside">
            <span className="tulisan">
              Apakah huruf ketiga terakhir dari alfabet?
            </span>
            <div id="box-choice">
              <div className="row" id="atas">
                <img
                  src={choiceB}
                  alt="Choice B"
                  className="image"
                  id="choice-b"
                  onClick={correctAnswer}
                />
                <img src={choiceC} alt="Choice C" className="image" />
              </div>
              <div className="row" id="bawah">
                <img src={choiceD} alt="Choice D" className="image" />
                <img src={choiceE} alt="Choice E" className="image" />
              </div>
            </div>
          </div>
        </div>
      </GameTujuhContainer>
    );
  }
}

GameTujuh.propTypes = {};

export default GameTujuh;
