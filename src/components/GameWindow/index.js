import React from "react";

import { GameWindowContainer } from "./style";

class GameWindow extends React.Component {
  render() {
    const { children } = this.props;
    return <GameWindowContainer>{children}</GameWindowContainer>;
  }
}

GameWindow.propTypes = {};

export default GameWindow;
