import React from "react";

import { GameDuaContainer } from "./style";
import pandaLeftDesktop from "../../asset/gamedualeft-desktop.png";
import pandaRightDesktop from "../../asset/gameduaright-desktop.png";
import pandaLeftMobile from "../../asset/gamedualeft-mobile.png";
import pandaRightMobile from "../../asset/gameduaright-mobile.png";

const GameDua = ({ correctAnswer, wrongAnswer }) => {
  const wrongAnswerFunc = e => {
    const outsideAnswerArea = !document
      .getElementById("spanC")
      .contains(e.target);
    if (outsideAnswerArea) {
      wrongAnswer();
    }
  };

  return (
    <GameDuaContainer>
      <div className="outer-container" onClick={wrongAnswerFunc}>
        <img
          src={pandaLeftDesktop}
          className="panda-left desktop"
          alt="panda-left"
        />
        <img
          src={pandaRightDesktop}
          className="panda-right desktop"
          alt="panda-right"
        />
        <img
          src={pandaLeftMobile}
          className="panda-left mobile"
          alt="panda-left"
        />
        <img
          src={pandaRightMobile}
          className="panda-right mobile"
          alt="panda-right"
        />
        <div className="answer-box">
          <div className="answer-col">
            <div className="answer-item" id="spanA">
              A. JYULE
            </div>
            <div className="answer-item" id="spanB">
              B. JYLEU
            </div>
          </div>
          <div className="answer-col">
            <div className="answer-item" id="spanC" onClick={correctAnswer}>
              C. JYLUE
            </div>
            <div className="answer-item" id="spanD">
              D. JUYLE
            </div>
          </div>
        </div>
      </div>
    </GameDuaContainer>
  );
};

export default GameDua;
