import styled from "styled-components";

export const GameDuaContainer = styled.div`
  background-color: #cedcb6;
  width: 100vw;
  min-height: 100vh;
  cursor: pointer;

  .panda-left {
    position: fixed;
    left: 0px;
    bottom: 0px;
    width: 80vw;
    max-height: 90vh;
  }

  .panda-right {
    position: fixed;
    right: 0px;
    bottom: 0px;
    width: 10vw;
  }

  .answer-box {
    display: flex;
    flex-direction: row;
    position: fixed;
    bottom: 5vw;
    left: 50%;
    transform: translateX(-50%);
  }

  .answer-col {
    display: flex;
    flex-direction: column;
    margin: 20px;
  }

  .answer-item {
    margin: 5px;
    padding: 10px;
    color: #0d2040;
    font-weight: 700;
    font-size: 3vw;
  }

  @media only screen and (max-width: 600px) {
    .desktop {
      display: none;
    }

    .answer-item {
      font-size: 5vw;
      padding: 5px 10px;
    }

    .answer-box {
      flex-direction: column;
      bottom: 30px;
      width: max-content;
    }

    .answer-col {
      margin-top: 0px;
      margin-bottom: 0px;
    }
  }

  @media only screen and (min-width: 601px) {
    .mobile {
      display: none;
    }
  }
`;
