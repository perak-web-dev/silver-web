import React from "react";

import { GameSembilanContainer } from "./style";
import bgTopDesktop from "../../asset/gamesembilantop-desktop.svg";
import bgBottomDesktop from "../../asset/gamesembilanbottom-desktop.svg";
import bgTopMobile from "../../asset/gamesembilantop-mobile.svg";
import bgBottomMobile from "../../asset/gamesembilanbottom-mobile.png";
import heliDesktop from "../../asset/gamesembilanheli-desktop.svg";
import heliMobile from "../../asset/gamesembilanheli-mobile.png";
import answerBg from "../../asset/gamesembilantext-bg.svg";
import answerBgMobile from "../../asset/gamesembilantext-bg-mobile.svg";
import tali from "../../asset/gamesembilan-tali.svg";

const GameSembilan = ({ correctAnswer, wrongAnswer }) => {
  const wrongAnswerFunc = e => {
    const outsideAnswerArea = !document
      .getElementById("span1")
      .contains(e.target);
    if (outsideAnswerArea) {
      wrongAnswer();
    }
  };

  return (
    <GameSembilanContainer>
      <div className="outer-container" onClick={wrongAnswerFunc}>
        <img
          src={bgTopDesktop}
          alt="background-top"
          className="background-top desktop"
        />
        <img
          src={bgTopMobile}
          alt="background-top"
          className="background-top mobile"
        />
        <img
          src={bgBottomDesktop}
          alt="background-bottom"
          className="background-bottom desktop"
        />
        <img
          src={bgBottomMobile}
          alt="background-bottom"
          className="background-bottom mobile"
        />
        <div className="main-content">
          <div className="question">
            Perak Fasilkom bertujuan untuk
            <div className="underline" />
            warga fasilkom
          </div>
          <div className="content-row">
            <div className="content-column">
              <img src={heliDesktop} alt="heli" className="heli desktop" />
              <img src={heliMobile} alt="heli" className="heli mobile" />
            </div>
            <div className="content-column">
              <div className="answer-item">
                <img
                  src={answerBg}
                  alt="ans-bg"
                  className="answer-bg desktop"
                />
                <img
                  src={answerBgMobile}
                  alt="ans-bg"
                  className="answer-bg mobile"
                />
                <div className="answer-content">
                  <div
                    id="span1"
                    className="answer-number"
                    style={{ backgroundColor: "#E9622A" }}
                    onClick={correctAnswer}
                  >
                    1
                  </div>
                  <div className="answer-text">TANDING FUTSAL</div>
                </div>
              </div>
              <img src={tali} alt="tali" className="answer-rope" />
              <div className="answer-item">
                <img
                  src={answerBg}
                  alt="ans-bg"
                  className="answer-bg desktop"
                />
                <img
                  src={answerBgMobile}
                  alt="ans-bg"
                  className="answer-bg mobile"
                />
                <div className="answer-content">
                  <div
                    className="answer-number"
                    style={{ backgroundColor: "#33B3A6" }}
                  >
                    2
                  </div>
                  <div className="answer-text">MENAMBAH TINGKAT BUCIN</div>
                </div>
              </div>
              <img src={tali} alt="tali" className="answer-rope" />
              <div className="answer-item">
                <img
                  src={answerBg}
                  alt="ans-bg"
                  className="answer-bg desktop"
                />
                <img
                  src={answerBgMobile}
                  alt="ans-bg"
                  className="answer-bg mobile"
                />
                <div className="answer-content">
                  <div
                    className="answer-number"
                    style={{ backgroundColor: "#FFA8BF" }}
                  >
                    3
                  </div>
                  <div className="answer-text">MENAMBAH KETENARAN</div>
                </div>
              </div>
              <img src={tali} alt="tali" className="answer-rope" />
              <div className="answer-item">
                <img
                  src={answerBg}
                  alt="ans-bg"
                  className="answer-bg desktop"
                />
                <img
                  src={answerBgMobile}
                  alt="ans-bg"
                  className="answer-bg mobile"
                />
                <div className="answer-content">
                  <div
                    className="answer-number"
                    style={{ backgroundColor: "#F2CF35" }}
                  >
                    4
                  </div>
                  <div className="answer-text">
                    MENGIRIM CUPID COURIER KE GEBETAN
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </GameSembilanContainer>
  );
};

export default GameSembilan;
