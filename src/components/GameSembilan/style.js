import styled from "styled-components";

export const GameSembilanContainer = styled.div`
  background-color: #454fcb;
  min-width: 100vw;
  min-height: 100vh;
  cursor: pointer;

  .question {
    color: white;
    font-weight: 700;
    font-size: 3vw;
    margin: 50px 0px;
    text-align: center;
    line-height: 140%;
  }

  .background {
    display: fixed;
    width: 100vw;
    height: 100vh;
  }

  .background-top {
    position: fixed;
    top: 0px;
    left: 0px;
    width: 100vw;
  }

  .background-bottom {
    position: fixed;
    bottom: 0px;
    left: 0px;
    width: 100vw;
  }

  .underline {
    border: 0px solid white;
    border-bottom-width: 5px;
    display: inline-block;
    width: 150px;
    margin: 0px 10px;
  }

  .main-content {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    z-index: 99;
    position: relative;
    padding-top: 10vh;
  }

  .content-row {
    display: flex;
    flex-direction: row;
  }

  .content-column {
    display: flex;
    flex-direction: column;
  }

  .answer-rope {
    width: 90%;
    margin: 0px auto;
  }

  .answer-item {
    position: relative;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
  }

  .answer-bg {
    position: absolute;
    min-height: 100%;
    min-width: 100%;
  }

  .answer-number {
    font-weight: 700;
    font-size: 20px;
    padding: 10px;
    border-radius: 100px;
    width: 30px;
    height: 30px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
  }

  .answer-content {
    position: relative;
    padding: 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
  }

  .answer-text {
    width: max-content;
    margin: 0px 50px 0px 20px;
    font-weight: 700;
    font-size: 20px;
  }

  @media only screen and (max-width: 600px) {
    .desktop {
      display: none;
    }
    .question {
      margin-top: 130px;
      font-size: 6vw;
    }
    .content-row {
      flex-direction: column;
    }
    .main-content {
      padding-bottom: 200px;
      padding-top: 0px;
    }
    .answer-item {
      margin-top: -10px;
      margin-bottom: -10px;
    }
    .answer-content {
      flex-direction: column;
      margin: 0px auto;
    }
    .answer-text {
      width: auto;
      margin: 10px 0px;
      text-align: center;
      font-size: 16px;
      width: 50vw;
    }
  }

  @media only screen and (min-width: 601px) {
    .mobile {
      display: none;
    }
  }
`;
