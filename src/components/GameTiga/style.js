import styled from "styled-components";
import gametiga from "../../asset/gametiga.svg";
import gametigamobile from "../../asset/gametigamobile.svg";

export const GameTigaStyled = styled.div`
  #game-3 {
    background-image: url(${gametiga});
    background-size: cover;
    background-position: center;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .text {
    font-family: "Montserrat", sans-serif;
    color: #454fcb;
    font-size: 50px;
    width: 500px;
    text-align: center;
  }

  /* Ipad */
  @media only screen and (max-width: 780px) {
    #game-3 {
      background-image: url(${gametigamobile}) !important;
    }
  }

  /* Mobile */
  @media only screen and (max-width: 430px) {
    .text {
      font-size: 32px;
      line-height: 39px;
      width: 200px;
    }
  }
`;
