import React from "react";
import { GameTigaStyled } from "./style";

const GameTiga = ({ correctAnswer, wrongAnswer }) => {
  const wrongAnswerFunc = e => {
    const outsideAnswerArea = !document
      .getElementById("span")
      .contains(e.target);
    if (outsideAnswerArea) {
      wrongAnswer();
    }
  };

  return (
    <GameTigaStyled>
      <div id="game-3" onClick={wrongAnswerFunc}>
        <h3 className="text">
          Carilah{" "}
          <span id="span" className="text" onClick={correctAnswer}>
            kesalahan
          </span>{" "}
          pada kalimat ini!
        </h3>
      </div>
    </GameTigaStyled>
  );
};

export default GameTiga;
