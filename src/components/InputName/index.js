import React from "react";

import { InputNameContainer } from "./style";

class InputName extends React.PureComponent {
  render() {
    const { handleInput, inputValue } = this.props;
    return (
      <InputNameContainer>
        <h5
          style={{ alignSelf: "center", marginBottom: 0, marginRight: "15px" }}
        >
          Name
        </h5>
        <input type="text" onChange={handleInput} value={inputValue} />
      </InputNameContainer>
    );
  }
}

export default InputName;
