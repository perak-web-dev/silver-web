import styled from "styled-components";
import no8 from "./no8.svg";

export const Container = styled.div`
  background: #e5e6de;
  padding: calc(1rem + 0.5vw);
  display: flex;
  flex-direction: column;
  border-radius: 2px;
  font-weight: bold;
  font-size: calc(1vw + 1rem);
  color: #e9622a;
`;

export const Row = styled.div`
  padding: 1rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  .smol {
    color: #e5e6de;
    cursor: pointer;
    font-size: 3rem;
    width: 5rem;
    @media (max-width: 600px) {
      font-size: 1.5rem;
      width: 3rem;
    }
  }
`;
const color = {
  yellow: "#F2CF35",
  purple: "#444fcb",
  orange: "#E9622A",
  green: "#33B3A6"
};

export const Box = styled.div`
  color: black;
  background: ${props => color[props.color]};
  padding: 1rem;
  margin: 0.5rem;

  justify-content: center;
  align-items: center;
  width: 8rem;
  @media (max-width: 600px) {
    width: 6rem;
  }
`;

export const Parampa8Container = styled.div`
  background: url(${no8});
  background-position: center;
  height: 100vh;
  background-repeat: no-repeat;
  background-size: cover;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 0 calc(1vw + 5rem);
  @media (max-width: 600px) {
    padding: 10px;
  }
  justify-content: center;
`;

export const Modal = styled.div`
  display: ${props => (props.open ? "flex" : "none")};
  overflow: hidden;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  z-index: 20;
  background-color: rgba(0, 0, 0, 0.4);
  // background: url("https://i.pinimg.com/originals/50/8f/e7/508fe7a0908b87ea1de36391094a1049.gif");
`;
