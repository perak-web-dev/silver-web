import React from "react";

import { Parampa8Container, Container, Box, Row } from "./style";

const Parampa8 = ({ wrongAnswer, correctAnswer }) => {
  // const [modal, setModal] = useState(false);
  // function timeout(delay) {
  //   return new Promise(res => setTimeout(res, delay));
  // }
  const handleAnswer = e => {
    e.stopPropagation();
    correctAnswer();
  };
  // const handleWrong = async () => {
  //   setModal(true);
  //   await timeout(2500);
  //   setModal(false);
  // };
  return (
    <Parampa8Container onClick={wrongAnswer}>
      {/* {modal ? (
        <Modal open={modal}>
          <img
            src="https://i.pinimg.com/originals/50/8f/e7/508fe7a0908b87ea1de36391094a1049.gif"
            alt="youshallnotpass"
          />
        </Modal>
      ) : ( */}
      <Container>
        Perhatikan Angka Di Bawah Ini!
        <Row>
          <Box color="orange">1 = 3</Box>
          <Box color="yellow">2 = 3</Box>
          <Box color="orange">3 = 3</Box>
          <Box color="yellow">4 = 3</Box>
          <Box color="orange">5 = 3</Box>
          <Box color="yellow">6 = ?</Box>
        </Row>
        <Row>
          <Box className="smol" color="orange">
            2
          </Box>
          <Box className="smol" onClick={handleAnswer} color="purple">
            3
          </Box>
          <Box className="smol" color="green">
            4
          </Box>
          <Box className="smol" color="orange">
            5
          </Box>
        </Row>
      </Container>
      {/* )} */}
    </Parampa8Container>
  );
};

Parampa8.propTypes = {};

export default Parampa8;
