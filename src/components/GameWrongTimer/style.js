import styled from "styled-components";

export const GameWrongTimerContainer = styled.div`
  background-color: #e9622a;
  justify-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;
  color: #e5e6de;
  width: 100vw;
  height: 100vh;
  position: fixed;
  font-size: 1rem;
  @media (max-width: 500px) {
    font-size: 0.9rem;
  }
  @media (max-width: 400px) {
    font-size: 0.75rem;
  }
  h1 {
    font-size: 2em;
  }
  .ok {
    height: 6em;
    width: auto;
  }
  .time {
    font-size: 3em;
  }
`;
