import React, { useState, useEffect } from "react";
import { ReactComponent as SmileOK } from "../../asset/smileOk.svg";

import { GameWrongTimerContainer } from "./style";

/**
 * kasi props timer buat set berapa lama timernya (detik)
 * kasi props closeCallback buat fungsi close nya dari parent
 */
const GameWrongTimer = props => {
  const { timer, closeCallback } = props;
  const [time, setTime] = useState(timer || 5);
  useEffect(() => {
    if (time > 0) {
      setTimeout(() => setTime(time - 1), 1000);
    } else if (closeCallback) {
      closeCallback();
    }
  }, [time]);
  return (
    <GameWrongTimerContainer>
      <h1>Ayo, coba lagi!</h1>
      <SmileOK className="ok" />
      <h1 className="time">{time}</h1>
    </GameWrongTimerContainer>
  );
};

export default GameWrongTimer;
