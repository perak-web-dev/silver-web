import styled from "styled-components";

export const InputUserGameContainer = styled.div`
  background: #f2cf35;
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  font-family: Montserrat, serif;
`;

export const InputName = styled.input`
  width: 100%;
  border: solid black 1.5px;
  border-radius: 5px;
  box-sizing: border-box;
  :focus {
    outline: none;
    padding-left: 5px;
  }
`;

export const LineEmail = styled.h3`
  font-size: 1.2rem;
  font-weight: bold;
  padding-left: 10px;
`;

export const MulaiButton = styled.button`
  width: 6rem;
  height: 2rem;
  background-color: #e9622a;
  border-radius: 5px;
  border: black solid 1.5px;
  color: white;
  margin-top: 1.5rem;
  margin-left: 0.5rem;
  margin-right: 0.5rem;
  align-self: center;
  font-weight: bold;
  font-size: 1rem;

  &.start:disabled {
    background-color: #ee8b5f;
    width: 7.5rem;
  }
`;

export const LineEmailContainer = styled.div`
  width: 100%;
  max-width: inherit;
  align-self: center;
  padding: 0 20%;
  margin-top: 0.4rem;
`;

export const UserInputContainer = styled.div`
  align-self: center;
  display: flex;
  flex-direction: column;
  max-width: 1000px;
  width: 100%;
`;
