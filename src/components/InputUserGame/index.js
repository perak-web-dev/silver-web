import React from "react";

import {
  InputName,
  InputUserGameContainer,
  LineEmail,
  LineEmailContainer,
  MulaiButton,
  UserInputContainer
} from "./style";
import title from "../../asset/perakmpa/titleLogin.svg";
import HeaderFooter from "../HeaderFooter";

class InputUserGame extends React.Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();
  }

  componentDidMount() {
    this.input.current.focus();
  }

  inputSection = () => {
    const { value, onChange } = this.props;
    return (
      <LineEmailContainer>
        <LineEmail>Line / Email</LineEmail>
        <InputName
          ref={this.input}
          type="text"
          value={value}
          onChange={onChange}
        />
      </LineEmailContainer>
    );
  };

  buttonSection = (loggedIn, start, logout, done) => {
    const { loading } = this.props;
    return (
      <div
        style={{ display: "flex", flexDirection: "row", alignSelf: "center" }}
      >
        <MulaiButton
          className="start"
          onClick={start}
          disabled={done || loading}
        >
          {done ? "Sudah Main" : loading ? "Loading..." : "Mulai"}
        </MulaiButton>
        {loggedIn && <MulaiButton onClick={logout}>Logout</MulaiButton>}
      </div>
    );
  };

  render() {
    const { loggedIn, name, start, logout, done, launched } = this.props;
    return (
      <HeaderFooter color="dark" noQuiz>
      <InputUserGameContainer className="games-app">
        <UserInputContainer>
          <img
            style={{
              maxWidth: "inherit",
              width: "100%",
              height: "auto",
              padding: "0 10%"
            }}
            src={title}
            alt="Title"
          />
          {launched && !loggedIn && this.inputSection()}
          {launched && loggedIn && <h3 align="center">Halo, {name}</h3>}
          {!launched && <h3 align="center">Belum di launch nih!</h3>}
          {launched && this.buttonSection(loggedIn, start, logout, done)}
        </UserInputContainer>
      </InputUserGameContainer>
      </HeaderFooter>
    );
  }
}

export default InputUserGame;
