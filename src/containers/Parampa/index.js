import React from "react";
// import PropTypes from "prop-types";
import { connect } from "react-redux";
// import Parampa8 from "../../components/Parampa8";
import Parampa10 from "../../components/Parampa10";
import HeaderFooter from "../../components/HeaderFooter";

import { ParampaContainer } from "./style";

const Parampa = () => {
  return (
    <HeaderFooter color="dark">
      <ParampaContainer>
        <Parampa10 />
      </ParampaContainer>
    </HeaderFooter>
  );
};

Parampa.propTypes = {};

function mapStateToProps(state) {
  return {
    state
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Parampa);
