import GameNo1 from "../../components/GameNo1";
import GameDua from "../../components/GameDua";
import GameTiga from "../../components/GameTiga";
import GameEmpat from "../../components/GameEmpat";
import GameNo5 from "../../components/GameNo5";
import GameEnam from "../../components/GameEnam";
import GameTujuh from "../../components/GameTujuh";
import Parampa8 from "../../components/Parampa8";
import GameSembilan from "../../components/GameSembilan";
import Parampa10 from "../../components/Parampa10";

const ListPage = [
  {
    time: 300,
    date: new Date().getDate(),
    page: [ GameNo1, GameDua, GameTiga, GameEmpat, GameNo5, GameEnam, GameTujuh, Parampa8, GameSembilan, Parampa10 ]
  },
  {
    time: 300,
    date: new Date().getDate() + 1,
    page: [ GameNo1, GameDua, GameTiga, GameEmpat, GameNo5, GameEnam, GameTujuh, Parampa8, GameSembilan, Parampa10 ]
  },
  {
    time: 300,
    date: new Date().getDate() + 2,
    page: [ GameNo1, GameDua, GameTiga, GameEmpat, GameNo5, GameEnam, GameTujuh, Parampa8, GameSembilan, Parampa10 ]
  }
];

export default ListPage;
