import axios from "axios";

const API_URL =
  "https://cors-anywhere.herokuapp.com/https://perakgame.herokuapp.com";

const post = (url, data) => {
  console.log(data);
  return axios.post(`${API_URL + url}`, JSON.stringify(data), {
    headers: {
      "content-type": "application/json"
    }
  });
};

export const SubmitGameData = name =>
  post("/game/start/", {
    kontak: name
  });

export const FinishGame = (correct, name, time, token) =>
  post("/game/finish/", {
    kontak: name,
    token,
    time,
    benar: correct
  });
