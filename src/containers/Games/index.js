import React from "react";
import { connect } from "react-redux";
import { GamesContainer } from "./style";
import InputUserGame from "../../components/InputUserGame";
import FinishedGame from "../../components/FinishedGame";
import ListPage from "./page";
import { FinishGame, SubmitGameData } from "./api";
import GameWrongTimer from "../../components/GameWrongTimer";

const STATE_CACHE_NAME = `fkdjaskgjkasjf12312412kcjkdjfas`;

class Games extends React.PureComponent {
  constructor(props) {
    super(props);

    // this.date = new Date();
    this.gameData = null;
    this.time = -1;
    this.correct = -1;

    this.state = {
      startDate: null,
      timer: -1,
      name: "",
      loggedIn: false,
      phase: -1,
      loggedInName: "",
      started: false,
      finished: false,
      correct: 0,
      wrong: false,
      done: false,
      token: "",
      launched: true,
      loading: false,
      dataSent: false
    };

    document.getElementsByTagName("html")[0].classList.add("games-app");

    this.interval = null;
  }

  componentWillUnmount() {
    document.getElementsByTagName("html")[0].classList.remove("games-app");
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      timer,
      finished,
      started,
      correct,
      loggedInName,
      done,
      name,
      loading,
      token,
      dataSent
    } = this.state;
    const doneFunc = () => {
      this.setState({
        finished: true,
        started: false
      });
    };

    if (done) {
      return;
    }

    if (
      name === prevState.name &&
      !loading &&
      loggedInName !== "" &&
      !started &&
      !finished &&
      token === ""
    ) {
      this.setState({
        loading: true
      });
      let nameDone;
      await this.alreadyPlayed(loggedInName).then(e => {
        nameDone = e;
        this.setState({
          loading: false
        });
      });

      if (nameDone) {
        this.setState({
          done: true
        });
        return;
      }
    }

    if (finished && !loading) {
      if (dataSent) {
        localStorage.removeItem(STATE_CACHE_NAME);
        return;
      }
      localStorage.setItem(STATE_CACHE_NAME, JSON.stringify(this.state));
      this.submitAnswer().then(e =>
        this.setState({
          dataSent: true
        })
      );
      return;
    }

    if (timer >= 0 && correct === this.correct) {
      doneFunc();
      return;
    }

    if (timer > 0 && started) {
      localStorage.setItem(STATE_CACHE_NAME, JSON.stringify(this.state));
      return;
    }

    if (timer === 0 && started) {
      doneFunc();
    }
  }

  componentDidMount() {
    const stateBefore = localStorage.getItem(STATE_CACHE_NAME);
    if (stateBefore) {
      const state = JSON.parse(stateBefore);
      if (state.finished) {
        this.setState(state);
        return;
      }
      this.loadPhase(state.phase);
      const timeFromStart =
        Math.round(
          (Date.parse(new Date()) - Date.parse(state.startDate)) / 100
        ) / 10;
      state.timer = this.time - timeFromStart;
      if (state.timer <= 0) {
        state.finished = true;
        state.started = false;
        this.setState(state);
        return;
      }
      this.setState(state);
      this.startTimer();
      return;
    }
    const name = localStorage.getItem("perakGames");

    if (name) {
      this.setState({
        loggedIn: true,
        loggedInName: name
      });
    }
  }

  loadPhase = phase => {
    this.gameData = ListPage[phase - 1];
    this.time = this.gameData.time;
    this.correct = this.gameData.page.length;
  };

  alreadyPlayed = async loggedInName => {
    const check = async () => {
      let already = false;
      await SubmitGameData(loggedInName)
        .then(({ data }) => {
          const phase = data.phase;
          this.loadPhase(phase);
          this.setState({
            startDate: new Date(),
            timer: this.time,
            phase,
            token: data.token
          });
        })
        .catch(({ response }) => {
          already = true;
          if (response.status === 403) {
            if (response.data.message === "Anda sudah Bermain hari ini") {
              this.setState({
                done: true
              });
              return;
            }
            this.setState({
              launched: false
            });
          }
        });
      return already;
    };
    return await check();
  };

  handleName = e => {
    this.setState({
      name: e.target.value
    });
  };

  decreaseTimer = () => {
    const { timer } = this.state;
    this.setState({
      timer: Math.round((timer - 0.1) * 10) / 10
    });
  };

  saveName = () => {
    const { name } = this.state;
    this.setState({
      loggedIn: true,
      loggedInName: name
    });
  };

  startTimer = () => {
    this.interval = setInterval(this.decreaseTimer, 100);
  };

  stopTimer = () => {
    clearInterval(this.interval);
  };

  submitAnswer = async () => {
    this.stopTimer();
    const { correct, loggedInName, timer, token } = this.state;
    this.setState({
      loading: true
    });
    await FinishGame(correct, loggedInName, this.time - timer, token).then(e =>
      this.setState({
        loading: false
      })
    );
  };

  correctAnswer = () => {
    const { correct } = this.state;
    this.setState({
      correct: correct + 1
    });
  };

  wrongAnswer = () => {
    this.setState({
      wrong: true
    });
  };

  doneWrong = () => {
    this.setState({
      wrong: false
    });
  };

  logout = () => {
    localStorage.removeItem("perakGames");
    this.setState({
      name: "",
      loggedIn: false,
      loggedInName: "",
      done: false,
      loading: false
    });
  };

  startGame = async () => {
    const { name, loggedIn } = this.state;
    if (!loggedIn) {
      if (name === "") {
        return;
      }
      localStorage.setItem("perakGames", name);
      this.setState({
        loading: true
      });
      const done = await this.alreadyPlayed(name);
      if (done) {
        this.setState({
          done: true,
          loggedIn: true,
          loggedInName: name
        });
        return;
      }
      this.saveName();
      this.setState({
        loading: false
      });
    }
    this.setState({
      started: true
    });
    this.startTimer();
  };

  render() {
    const {
      name,
      loggedIn,
      started,
      finished,
      correct,
      timer,
      loggedInName,
      wrong,
      done,
      launched,
      loading
    } = this.state;
    const GamePage =
      this.gameData !== null
        ? this.gameData.page[
            correct === this.correct ? this.correct - 1 : correct
          ]
        : () => <></>;
    return (
      <GamesContainer>
        {started && (
          <h3 style={{ position: "absolute", top: 0, zIndex: 10000 }}>
            {timer}
          </h3>
        )}
        {!started && !finished && (
          <InputUserGame
            value={name}
            onChange={this.handleName}
            loggedIn={loggedIn}
            name={loggedInName}
            start={this.startGame}
            done={done}
            logout={this.logout}
            launched={launched}
            loading={loading}
          />
        )}
        {started && !wrong && (
          <GamePage
            timer={timer}
            correctAnswer={this.correctAnswer}
            wrongAnswer={this.wrongAnswer}
          />
        )}
        {finished && <FinishedGame name={loggedInName} loading={loading} />}
        {wrong && !finished && started && (
          <GameWrongTimer closeCallback={this.doneWrong} />
        )}
      </GamesContainer>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Games);
